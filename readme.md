# Ateliers Ideme

[ateliersideme.fr](https://ateliersideme.fr)

## Install

```sh
npm install
```

## Build

```sh
npm run build
```

## Serve

```sh
npm run serve
```
