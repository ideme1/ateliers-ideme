function displayMenu() {
  display('menu-main-content');
  display('menu-weil');
}

function hideMenu() {
  hide('menu-main-content');
  hide('menu-weil');
}

function display(id) {
  const element = document.getElementById(id);

  if (element) {
    element.classList.add('-displayed');
  }
}

function hide(id) {
  const element = document.getElementById(id);

  if (element) {
    element.classList.remove('-displayed');
  }
}

const carouselImages = Array.from(document.getElementsByClassName('carousel--image'));

carouselImages.forEach(image => image.addEventListener('click', () => {
  carouselImages.filter(other => other !== image).forEach(other => other.classList.remove('-selected'));

  if(!image.classList.contains('-selected')) {
    image.classList.add('-selected');
  } else {
    image.classList.remove('-selected');
  }
}))
